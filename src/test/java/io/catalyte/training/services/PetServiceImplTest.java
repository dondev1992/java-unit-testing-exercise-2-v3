package io.catalyte.training.services;

import io.catalyte.training.entities.Pet;
import io.catalyte.training.entities.Vaccination;
import io.catalyte.training.exceptions.BadDataResponse;
import io.catalyte.training.exceptions.ResourceNotFound;
import io.catalyte.training.exceptions.ServiceUnavailable;
import io.catalyte.training.repositories.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doThrow;

class PetServiceImplTest {

    @Mock
    PetRepository petRepository;

    @InjectMocks
    PetServiceImpl petServiceImpl;

    Pet testPet1;
    Pet testPet2;
    Pet testPet3;

    List<Pet> testPets = new ArrayList<>();


    @BeforeEach
    @SuppressWarnings("unchecked")
    public void setUp() {

        MockitoAnnotations.openMocks(this);

        //set up test Pet
        testPet1 = new Pet("Duke", "Dog", 1);
        testPet1.setId(1L);
        testPets.add(testPet1);

        testPet2 = new Pet("Monster", "Gecko", 8);
        testPet2.setId(2L);
        testPets.add(testPet2);

        testPet3 = new Pet("Gator", "Alligator", 71);
        testPet3.setId(3L);
        testPets.add(testPet3);

        when(petRepository.findById(any(Long.class))).thenReturn(Optional.of(testPets.get(0)));
        when(petRepository.saveAll(anyCollection())).thenReturn(testPets);
        when(petRepository.save(any(Pet.class))).thenReturn(testPets.get(0));
        when(petRepository.findAll()).thenReturn(testPets);
        when(petRepository.findAll(any(Example.class))).thenReturn(testPets);
    }

    @Test
    public void getAllPets() {
        List<Pet> result = petServiceImpl.queryPets(new Pet());
        assertEquals(testPets, result);
    }

    @Test
    public void getAllPetsWithSample() {
        List<Pet> result = petServiceImpl.queryPets(testPet1);
        assertEquals(testPets, result);
    }

    @Test
    public void getAllPetsDBException() {
        // set repo to trigger Data Access Exception
        when(petRepository.findAll()).thenThrow(EmptyResultDataAccessException.class);

        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.queryPets(new Pet()));
    }

    @Test
    public void getPet() {
        Pet result = petServiceImpl.getPet(1L);
        assertEquals(testPet1, result);
    }

    @Test
    public void getPetDBException() {
        when(petRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.getPet(1L));
    }

    @Test
    public void getPetNotFound() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.getPet(1L));
        String expectedMessage = "Could not locate a Pet with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void addPets() {
        List<Pet> result = petServiceImpl.addPets(testPets);
        assertEquals(testPets, result);
    }

    @Test
    public void addPetsDBException() {
        when(petRepository.saveAll(anyCollection())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));

        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.addPets(testPets));
    }

    @Test
    public void addPet() {
        Pet result = petServiceImpl.addPet(testPet1);
        assertEquals(testPet1, result);
    }

    @Test
    public void addPetDBException() {
        when(petRepository.save(any(Pet.class))).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.addPet(testPet1));
    }

    @Test
    public void updatePetById() {
        Pet result = petServiceImpl.updatePetById(1L, testPet1);
        assertEquals(testPet1, result);
    }

    @Test
    public void updatePetByIdDBError() {
        when(petRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.updatePetById(1L, testPet1));
    }

    @Test
    public void updatePetByIdNotFound() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.updatePetById(1L, testPet1));
        String expectedMessage = "Could not locate a Pet with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updatePetByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> petServiceImpl.updatePetById(2L, testPet1));
        String expectedMessage = "Pet ID must match the ID specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deletePet() {
        when(petRepository.existsById(anyLong())).thenReturn(true);
        petServiceImpl.deletePet(1L);
        verify(petRepository).deleteById(any());
    }

    @Test
    public void deletePetBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(petRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.deletePet(1L));
    }

    @Test
    public void deletePetDBError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(petRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.deletePet(1L));
    }

}